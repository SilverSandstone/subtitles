Subtitles Changelog
===================

0.4.1 (2024-05-28)
------------------

- Tweaked documentation.
- Removed unnecessary files from release archives.


0.4.0 (2024-05-24)
------------------

- Improved the Repixture UI style.
- Updated descriptions for Unified Inventory.
- Added more descriptions for Survivetest.
- Added more descriptions for Repixture.
- Added descriptions for Cave Ambience.
- Added descriptions for Simple Cave.
- Added descriptions for MineClone 2 C418 Records.
- Added descriptions for Arena_Lib.
- Added descriptions for Glitch.
- Fixed fire subtitles never expiring in MineClone (reported by MysticTempest).
- Tweaked the format of inline documentation.
- Added custom UI styles for Glitch.
- Changed unknown sounds from angle brackets to square brackets.


0.3.0 (2023-01-14)
------------------

- Added global settings to enable/disable subtitles by default on singleplayer and multiplayer.
- Added a global setting to enable/disable the introduction message.
- Added a global setting to enable/disable inventory integration.
- Node digging sounds are now decided based on groups in the absence of an explicit sound definition.
- Fixed player footsteps not having subtitles on older versions of Minetest.
- Fixed a crash on older versions of Minetest when hearing non-player footsteps.
- Added inventory integration for Repixture.
- Changed the `:add()`, `:remove()`, and `:update()` methods of `SubtitleDisplay` to `:add_sound()`, `:remove_sound()`, and `:update_sound()`.
- Converted the licence file from plain text to Markdown.
- Added descriptions for Subway Miner.
- Added descriptions for Advanced Trains Freight Train.
- Added descriptions for Automobiles Pack.
- Added descriptions for Bows.
- Added descriptions for Regional Weather.
- Added descriptions for i3.
- Added descriptions for Documentation System.
- Added descriptions for LineTrack.
- Added descriptions for Shifter Tool.
- Added descriptions for Mese Portals.
- Added descriptions for Bell.
- Added descriptions for New Fireworks.
- Added descriptions for BWeapons Modpack.
- Added descriptions for Radiant Damage.
- Added descriptions for Vacuum.
- Added descriptions for X Clay.
- Added descriptions for Scythes and Sickles.
- Added more descriptions for MineClone and Moontest.
- Added more descriptions for Sounds, Ambience, Mobs Creatures, and JR E231.
- Fixed some incorrect sound names.
- `find-missing.sh` now follows symlinks.
- Improved the visual consistency of the preferences menu on i3.


0.2.0 (2022-12-31)
------------------

- Subtitles are now enabled by default on singleplayer.
- Settings are now stored in the agent for improved performance.
- Tweaked some UI textures.
- Added custom UI styles for MineClone and Repixture.
- The mode list in the preferences menu is now scrollable.
- Changed the way sound directions are calculated.
- Reduced the number of HUDs used by the classic display mode.
- Players can now enable/disable subtitles for footsteps.
- Added `help` and `footsteps` subcommands to the `/subtitles` command.
- Added `find-missing.sh` script to list sounds without descriptions.
- Added descriptions for Technic.
- Added descriptions for Mesebox.
- Added descriptions for Storage Drawers.
- Added descriptions for Enderpearl.
- Added descriptions for Demoiselle, Ju52, PA28, Super Cub, Super Duck Hydroplane, and Ultralight Trike.
- Added descriptions for Fishing!.
- Added descriptions for Spyglass.
- Added descriptions for Super Sam.
- Added descriptions for Real Torch.
- Added descriptions for CCompass.
- Added descriptions for Death Compass.
- Added descriptions for NextGen Bows.
- Added descriptions for Pyramids.
- Added descriptions for Stamina.
- Added descriptions for Hunger.
- Added descriptions for Ambiance.
- Added descriptions for Anvil.
- Added descriptions for Castle Weapons.
- Added descriptions for Ropes.
- Added descriptions for Travelnet.
- Added more descriptions for Minetest Game, MineClone, NodeCore, Repixture, and Moontest.
- Added more descriptions for Mobs, Home Decor, and Vehicles.
- Clarified the descriptions of some sounds.
- Rearranged the cover image to improve visibility of the subtitles in the screenshots.
- Darkened the border of the classic subtitles.
- The introduction message is no longer shown on singleplayer.


0.1.1 (2022-12-27)
------------------

- Fixed the version number being incorrect.
- Fixed an incorrect date in the changelog.
- Re-added the missing screenshot.


0.1.0 (2022-12-26)
------------------

- Improved footstep detection when swimming or walking through plants.
- Added ‘Supporting Subtitles in Your Mod’ to the readme.
- Corner subtitles now show the sound's distance and direction.
- Added API documentation using LDoc.
- Fixed some incorrect function signatures.
- Changed the description of railway crossing bells (suggested by Y. Wang).
- Fixed a bug causing unnecessary HUD updates.
- Fixed a bug causing HUD updates to change defaults.
- Fixed another bug causing unnecessary HUD updates.
- Increased the width of corner subtitles.
- Added a build script.
- Added descriptions for Mobs Skeleton.
- Added descriptions for Creatura and Animalia.
- Added descriptions for X Enchanting.
- Added descriptions for More Trains.
- Added descriptions for Bedrock.
- Tweaked some UI textures.
- Improved the classic subtitle appearance on older clients.
- Various performance improvements.


0.0.1 (2022-12-24)
------------------

- Fixed a missing file (reported by Y. Wang).


0.0.0 (2022-12-22)
------------------

- Added everything.
