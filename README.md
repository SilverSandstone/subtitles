Subtitles
=========

[ContentDB](https://content.minetest.net/packages/SilverSandstone/subtitles/) ·
[Codeberg](https://codeberg.org/SilverSandstone/subtitles)

This Minetest mod adds on-screen subtitles for sound effects.

Subtitles can be toggled and configured for each player.
You can access the subtitles menu by clicking the ![Subtitles icon] button in the inventory or by typing `/subtitles` in chat.

Subtitles are disabled by default on multiplayer.


Features
--------

- Descriptions for over 1,500 sound effects.
- Multiple styles of subtitles.
- Direction indicators for positional sounds.
- Separate preferences for each player.
- UI integration for supported inventories.
- A simple API for adding subtitles to your own game or mod.


Supported Games and Mods
------------------------

Descriptions are provided for these games:

- Alter
- Builda City
- Glitch
- Jail Escape
- Klots
- Mineclonia
- Minetest Game
- Moontest
- NodeCore
- Repixture
- Shadow Forest
- Subway Miner
- Super Sam
- The Unexpected Gambit
- VoxeLibre

Descriptions are provided for these mods:

- Advanced Trains
    - Basic Trains
    - DlxTrains
    - Freight Train
    - Japanese Tram TLR0600
    - JR E231
    - More Trains
    - Neat Trains
- Ambience
- Anvil
- APercy's aeroplanes
    - Demoiselle
    - Ju52
    - PA28
    - Super Cub
    - Super Duck Hydroplane
    - Ultralight Trike
- Arena_Lib
- Automobiles Pack
- Awards
- Bedrock
- Bees
- Bell
- Bows
- BWeapons Modpack
- Castle Weapons
- Creatura
    - Animalia
- Death Compass
- DFCaverns
- Digtron
- Documentation System
- Draconis
- CCompass
- Enderpearl
- Ethereal
- Everness
- Fishing!
- Gunslinger
- Home Decor
- Hudbars
- i3
- Mesebox
- Mesecons
- Mese Portals
- MineClone 2 C418 Records
- Minetest Game Plus
- Mobs
    - Mobs Animal
    - Mobs Monster
    - Mobs Creature
    - Mobs Skeleton
    - Mobs MC
    - Mob Horse
    - Extra Mobs
    - Slimes Redo
- Nether
- New Fireworks
- NextGen Bows
- Pedology
- Pride Flags
- Pyramids
- Radiant Damage
- Real Torch
- Regional Weather
- Ropes
- Scythes and Sickles
- Shifter Tool
- Sounds
- Spyglass
- Stamina
- Steampunk Blimp
- Storage Drawers
- Torch Bomb
- TPH's Spyglass
- Travelnet
- Unified Inventory
- Vacuum
- Vehicles
- Weather
- X Clay
- X Enchanting

UI integration is provided for these games and mods:

- Simple Fast Inventory (via SFInv Buttons)
- Unified Inventory
- i3
- Repixture


Supporting Subtitles in Your Mod
--------------------------------

### Registration API

You can register a description to be used whenever a particular sound is
played using the `subtitles.register_description` function:

	subtitles.register_description('default_dig_metal', S'Metal clangs');


### Extended Sound Parameters

You can specify a description by setting `description` or `subtitle` in either
the sound spec or the parameters when playing a sound:

	minetest.sound_play('default_dig_metal', {gain = 0.5, description = S'Metal clangs'});
	minetest.sound_play({name = 'default_dig_metal', description = S'Metal clangs'}, {gain = 0.5});

This also works in node definitions:

	minetest.register_node('foomod:foobarium',
	{
		description = S'Foobarium';
		sounds =
		{
		    dig      = {name = 'default_dig_metal', description = S'Foobarium breaks'};
		    footstep = {name = 'default_dig_metal', description = S'Footsteps on foobarium'};
		};
	});

You can specify how long to display the subtitle by setting `duration` on
either the spec or the parameters:

    minetest.sound_play('default_dig_cracky', {duration = 2.5});

You can disable the subtitle for a sound by setting `no_subtitle = true` on
the spec or parameters, or by setting the description to an empty string:

    minetest.sound_play('foomod_music_1', {no_subtitle = true});
    minetest.sound_play('foomod_music_2', {description = ''});


Licence
-------

Subtitles by Silver Sandstone is licensed under the MIT licence, with assets under CC0.

See `LICENCE.md` for more information.


[Subtitles icon]: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAACVBMVEUAAAAAAAD///+D3c/SAAAAAXRSTlMAQObYZgAAAC5JREFUCFtjYEAFrKGhAQxSq1YtQSckp6WmQFmR06As1tDMAKCeFJBGEIPBgQEAwEwSV24kTgQAAAAASUVORK5CYII=
 "Subtitles"
