--[[
    Subtitles — adds subtitles to Minetest.

    Copyright © 2022‒2024, Silver Sandstone <@SilverSandstone@craftodon.social>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
]]


--- Public API functions.
-- @module api


local S = subtitles.S;


--- A list of {pattern: string, description: string|nil, parameters: table|nil}.
-- If you modify this manually, you must call `subtitles.invalidate_registered_properties_cache`.
subtitles.registered_properties = {};

--- A table mapping player names to Agent objects.
subtitles.agents_by_player = {};

--- A set of sound names that have been reported with `subtitles.report_missing`.
subtitles.reported_missing = {};


local g_registered_parameters_cache = {};
local g_registered_descriptions_cache = {};


--- Handles a call to minetest.sound_play(), or an equivalent event.
-- @param spec       [SimpleSoundSpec] The sound being played.
-- @param parameters [table|nil]       The sound parameters.
-- @param handle     [integer|nil]     The sound's handle, or nil if the sound is ephemeral.
function subtitles.on_sound_play(spec, parameters, handle)
    local sound = subtitles.Sound(spec, parameters, handle);
    if sound:is_exempt() then
        return;
    end;
    for _, player in ipairs(sound:get_players()) do
        local agent = subtitles.get_agent(player);
        if agent:get_enabled() and sound:is_in_range_of_player(player) then
            local display = agent:get_display();
            display:handle_sound_play(sound);
        end;
    end;
end;


--- Handles a call to minetest.sound_stop(), or an equivalent event.
-- @param handle [integer] The sound's handle.
function subtitles.on_sound_stop(handle)
    for username, agent in pairs(subtitles.agents_by_player) do
        agent:get_display():handle_sound_stop(handle);
    end;
end;


--- Returns the Agent object associated with the specified player.
-- The agent is created if necessary.
-- @param player [ObjectRef|string] The username of ref of a connected player.
-- @return       [Agent]            The player's agent.
function subtitles.get_agent(player)
    if minetest.is_player(player) then
        player = player:get_player_name();
    end;

    local agent = subtitles.agents_by_player[player];
    if not agent then
        agent = subtitles.Agent(player);
        subtitles.agents_by_player[player] = agent;
    end;
    return agent;
end;


--- Handles a node action.
-- @param pos    [vector] The position of the node, as an integer vector.
-- @param node   [Node]   The node table or node name.
-- @param action [string] A key in the `sounds` table, such as 'dig' or 'footstep'.
function subtitles.on_node_action(pos, node, action)
    if type(node) ~= 'string' then
        node = node.name;
    end;

    local spec = subtitles.util.get_node_sound(node, action);
    if spec then
        local parameters = {pos = pos, duration = subtitles.EPHEMERAL_DURATION};
        subtitles.on_sound_play(spec, parameters, nil);
    end;
end;


--- Invalidates the caches used for registered parameters and descriptions.
-- This must be called after modifying `subtitles.registered_properties`.
function subtitles.invalidate_registered_properties_cache()
    g_registered_parameters_cache = {};
    g_registered_descriptions_cache = {};
end;


--- Registers a description associated with a sound name.
-- When this sound is played, it will have the specified description unless a description is passed in the spec or parameters.
-- @param pattern     [string]     A regular expression matching the technical name of the sound, without the extension or variant suffix.
-- @param description [string|nil] The human-readable description of the sound, or nil to not associate a description.
-- @param parameters  [table|nil]  An optional table of parameters to use when playing this sound.
function subtitles.register_description(pattern, description, parameters)
    table.insert(subtitles.registered_properties, {pattern = pattern, description = description, parameters = parameters});
    subtitles.invalidate_registered_properties_cache();
end;


--- Retrieves a sound's registered description and parameters.
-- @param name [string]     The technical name of the sound.
-- @return     [string|nil] The sound's registered description, if any.
-- @return     [table]      A compiled table of all applicable registered parameters.
function subtitles.get_registered_properties(name)
    local compiled = g_registered_parameters_cache[name];
    if compiled then
        return g_registered_descriptions_cache[name], compiled;
    end;

    compiled = {};
    local description;
    for _, item in ipairs(subtitles.registered_properties) do
        if name:match('^' .. item.pattern .. '$') then
            description = item.description or description;
            if item.parameters then
                for key, value in pairs(item.parameters) do
                    compiled[key] = value;
                end;
            end;
        end;
    end;
    g_registered_parameters_cache[name] = compiled;
    g_registered_descriptions_cache[name] = description;
    return description, compiled;
end;


--- Reports a missing subtitle to the debug log.
-- @param name [string] The technical name of the sound.
function subtitles.report_missing(name)
    if not subtitles.reported_missing[name] then
        subtitles.reported_missing[name] = true;
        minetest.log('warning', ('[Subtitles] No description available for sound ‘%s’.'):format(name));
    end;
end;
