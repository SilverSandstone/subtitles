--[[
    Subtitles — adds subtitles to Minetest.

    Copyright © 2022‒2024, Silver Sandstone <@SilverSandstone@craftodon.social>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
]]


--- Provides the Sound class, which represents a playing sound effect.
-- @module Sound


local vector_copy = vector.copy or vector.new;


--- Represents a playing sound effect.
-- Sound objects should be treated as immutable, as they are shared between
-- all players.
-- @type Sound
subtitles.Sound = subtitles.Object:extend();

--- Constructor.
-- @param spec       [SimpleSoundSpec] The sound's spec, as a table or string.
-- @param parameters [table|nil]       The sound's parameters.
-- @param handle     [integer|nil]     The sound's numeric handle, or nil if the sound is ephemeral.
function subtitles.Sound:new(spec, parameters, handle)
    if type(spec) == 'string' then
        spec = {name = spec};
    end;
    parameters = parameters or {};

    local registered_description, registered_parameters = subtitles.get_registered_properties(spec.name or '');

    self.spec = spec or {};
    self.parameters = subtitles.util.update({}, registered_parameters, parameters);
    self.handle = handle;
    self.ephemeral = not handle;

    self.description = parameters.subtitle
                    or parameters.description
                    or spec.subtitle
                    or spec.description
                    or registered_description;

    -- Copy the position vector in case something else modifies it:
    self.parameters.pos = self.parameters.pos and vector_copy(self.parameters.pos);
end;

--- Checks which players the sound is audible to.
-- @return [table] A sequence of player ObjectRefs.
function subtitles.Sound:get_players()
    if self.parameters.to_player then
        return {minetest.get_player_by_name(self.parameters.to_player)};
    elseif self.parameters.exclude_player then
        local players = {};
        for _, player in ipairs(minetest.get_connected_players()) do
            if player:get_player_name() ~= self.parameters.exclude_player then
                table.insert(players, player);
            end;
        end;
        return players;
    else
        return minetest.get_connected_players();
    end;
end;

--- Checks if this sound is within the player's range.
-- @param player [ObjectRef] The player to check.
-- @return       [boolean]   true if the sound is in range.
function subtitles.Sound:is_in_range_of_player(player)
    local sound_pos = self:get_pos();
    if not sound_pos then
        return true;
    end;
    local distance = vector.distance(player:get_pos(), sound_pos);
    return distance <= self:get_max_distance();
end;

--- Returns the sound's maximum subtitle distance.
-- @return [number] The maximum distance in metres.
function subtitles.Sound:get_max_distance()
    return tonumber(self.parameters.max_subtitle_distance)
        or tonumber(self.parameters.max_hear_distance)
        or subtitles.DEFAULT_MAX_HEAR_DISTANCE;
end;

--- Returns the sound's expected duration
-- @return [number|nil] The duration in seconds, or nil if the sound loops.
function subtitles.Sound:get_duration()
    local duration = tonumber(self.parameters.subtitle_duration)
                  or tonumber(self.parameters.duration)
                  or tonumber(self.spec.subtitle_duration)
                  or tonumber(self.spec.duration);
    if duration then
        return duration;
    end;

    if not self.parameters.loop then
        return subtitles.DEFAULT_DURATION;
    end;

    return nil;
end;

--- Returns the sound's description.
-- @return [string] A human-readable description.
function subtitles.Sound:get_description()
    -- Description specified in spec or parameters:
    local desc = self.description;
    if desc then
        return desc;
    end;

    -- Fallback — just show the technical name:
    local name = self:get_name();
    subtitles.report_missing(name);
    return '[' .. name .. ']';
end;

--- Returns the sound's technical name.
-- @return [string] A sound name string.
function subtitles.Sound:get_name()
    return self.spec.name or '';
end;

--- Calculates the sound's current position.
-- This may change if the sound is attached to an object.
-- @return [vector|nil] An absolute position vector.
function subtitles.Sound:get_pos()
    -- Object position:
    if self.parameters.object then
        local object_pos = self.parameters.object:get_pos();
        if object_pos then
            self.last_object_pos = object_pos;
            return object_pos;
        elseif self.last_object_pos then
            return self.last_object_pos;
        end;
    end;

    -- Position in parameters:
    if self.parameters.pos then
        return self.parameters.pos;
    end;

    return nil;
end;

--- Returns the sound's position from the player's perspective.
-- @param player [ObjectRef] The player.
-- @return       [vector]    A vector relative to the player's position and rotation.
function subtitles.Sound:get_relative_pos_for_player(player)
    local pos = self:get_pos();
    if not pos then
        return nil;
    end;

    local player_pos = player:get_pos();
    local player_yaw = player:get_look_horizontal() or 0.0;
    pos = vector.subtract(pos, player_pos);
    pos = vector.rotate(pos, vector.new(0, -player_yaw, 0));
    return pos;
end;

--- Returns the key to use for merging this sound with other sounds.
-- Multiple sounds with the same merge key will be merged into a single subtitle.
-- @return [any] A table key, or nil to not merge.
function subtitles.Sound:get_merge_key()
    local key = self.parameters.merge_subtitle
             or self.spec.merge_subtitle;
    if key == true then
        return self:get_name();
    elseif key then
        return key;
    end;

    if self.ephemeral then
        return self:get_name();
    end;

    return nil;
end;

--- Checks if this sound should be exempt from subtitles.
-- @return [boolean] true to disable the subtitle.
function subtitles.Sound:is_exempt()
    return self.spec.no_subtitle
        or self.parameters.no_subtitle
        or self:get_name() == ''
        or self:get_description() == '';
end;

--- Checks if this sound is dynamically positioned (attached to an object).
-- @return [boolean] true if the sound is dynamic.
function subtitles.Sound:is_dynamic()
    return self.params.object ~= nil;
end;
