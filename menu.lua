--[[
    Subtitles — adds subtitles to Minetest.

    Copyright © 2022‒2024, Silver Sandstone <@SilverSandstone@craftodon.social>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
]]


--- Implements the subtitle menu GUI.
-- @module menu


local S = subtitles.S;


local MAX_HEIGHT = 10.5;


--- Creates a menu formspec for the specified player.
-- @param player   [ObjectRef|string] The player the formspec is for.
-- @param embedded [boolean]          This should be true if the menu is being embedded in another formspec.
-- @param options  [table|nil]        A table of extra options: width, height.
-- @return         [string]           A formspec.
function subtitles.get_menu_formspec(player, embedded, options)
    local agent = subtitles.get_agent(player);
    player = agent.username;
    options = options or {};

    local _esc = minetest.formspec_escape;
    local natural_height = 3.75 + #subtitles.SubtitleDisplay.implementations * 1.25;
    local width = options.width or 8;
    local height = options.height or math.min(natural_height, MAX_HEIGHT);
    local display_name = agent:get_display_name();

    local formspec = {};
    local function _add(template, ...)
        formspec[#formspec + 1] = string.format(template, ...);
    end;

    if not embedded then
        _add('size[%f,%f]', width, height);
        _add('real_coordinates[true]');
    end;

    -- Style:
    local selection_colour = '#80808040';
    local panel = 'subtitles_panel.png;4';
    local disabled_mod = '^subtitles_slash.png';
    local state_icon = 'subtitles_icon.png';
    local footsteps_icon = 'subtitles_footsteps.png';
    local circle = 'subtitles_circle.png';

    if minetest.get_modpath('rp_formspec') then
        -- Repixture style:
        _add('background9[0,0;0,0;ui_formspec_bg_short.png^[resize:512x256;true;32] bgcolor[#00000000]');          -- Background
        _add('style_type[button;border=false]');                                                                   -- Reset buttons
        _add('style_type[button;bgimg=ui_button_1w_inactive.png^[resize:64x64;bgimg_middle=24,16,-24,-24]');       -- Passive button image
        _add('style_type[button:pressed;bgimg=ui_button_1w_active.png^[resize:64x64;bgimg_middle=24,24,-24,-16]'); -- Active button image
        selection_colour = '#A48E63';
        panel = 'subtitles_rp_panel.png^[resize:48x48;16';
        state_icon = 'subtitles_rp_state.png';
        footsteps_icon = 'subtitles_rp_footsteps.png';
        disabled_mod = '^[colorize:#74664B:255';
    elseif minetest.get_modpath('mcl_core') then
        -- MineClone style:
        panel = 'subtitles_mcl_panel.png;4';
    elseif minetest.get_modpath('glitch_gui') then
        -- Glitch style:
        _add('style_type[image_button;bgimg=glitch_gui_button.png;bgimg_middle=1]');
        _add('style_type[image_button:hovered;bgimg=glitch_gui_button_hovered.png;bgimg_middle=1]');
        _add('style_type[image_button:pressed;bgimg=glitch_gui_button_pressed.png;bgimg_middle=1]');
        panel = 'glitch_gui_button.png;2';
        selection_colour = '#003000';
    elseif embedded and minetest.get_modpath('i3') then
        -- i3 style:
        panel = 'subtitles_i3_panel.png;4';
        state_icon = 'subtitles_icon_flat_hd.png';
        footsteps_icon = 'subtitles_i3_footsteps.png';
        disabled_mod = '^[opacity:64';
        circle = 'subtitles_i3_circle.png';
    end;

    -- State:
    local enabled = agent:get_enabled();
    local state_label;
    local toggle_tooltip;
    if enabled then
        state_label = S'Subtitles enabled';
        toggle_tooltip = S'Click to disable subtitles';
    else
        state_label = S'Subtitles disabled';
        toggle_tooltip = S'Click to enable subtitles';
        state_icon = state_icon .. disabled_mod;
    end;

    local footsteps = agent:get_footsteps_enabled();
    local footsteps_tooltip;
    if footsteps then
        footsteps_tooltip = S'Footsteps enabled';
    else
        footsteps_tooltip = S'Footsteps disabled';
        footsteps_icon = footsteps_icon .. disabled_mod;
    end;

    -- Toggle button:
    _add('image_button[0.25,0.25;1,1;%s;toggle;]', _esc(state_icon));
    _add('tooltip[toggle;%s]', _esc(toggle_tooltip));
    -- State label:
    _add('hypertext[1.5,0.5;%f,1;state_label;<big><b>%s</b></big>]', width - 1.75, _esc(state_label));

    -- Mode panel:
    _add('image[0.25,2.0;%f,%f;%s]', width - 0.5, height - 3.5, panel);
    _add('label[0.25,1.75;%s]', _esc(S'Display mode:'));

    -- Footsteps button:
    _add('image_button[0.25,%f;1,1;%s;footsteps;]', height - 1.25, _esc(footsteps_icon));
    _add('tooltip[footsteps;%s]', _esc(footsteps and S'Footsteps enabled' or S'Footsteps disabled'));
    -- Exit button:
    _add('button_exit[%f,%f;3,1;done;%s]', width - 3.25, height - 1.25, _esc(S'Done'));

    -- Mode list:
    _add('scroll_container[0.25,2.0;%f,%f;mode_list_scrollbar;vertical;]', width - 0.5, height - 3.5);
    _add('scrollbaroptions[max=%d]', math.max(0, natural_height - height));
    _add('scrollbar[%f,2.0;0.5,%f;vertical;mode_list_scrollbar;]', width - 0.5, height - 3.5);

    -- Display modes:
    for index, impl in ipairs(subtitles.SubtitleDisplay.implementations) do
        local y = 0.25 + (index - 1) * 1.25;
        if impl.NAME == display_name then
            local colour = enabled and '#00FF00' or '#BFBFBF';
            _add('box[0.125,%f;%f,1.25;%s]', y - 0.125, width - 0.75, selection_colour);
            _add('image[%f,%f;1,1;%s^[multiply:%s]', width - 1.75, y, circle, colour);
        end;
        _add('image[0.25,%f;1,1;%s]', y, _esc(impl.ICON));
        _add('button[1.5,%f;%f,1;display_%s;%s]', y, width - 3.5, impl.NAME, _esc(impl.TITLE));
        _add('tooltip[display_%s;%s]', impl.NAME, _esc(impl.DESCRIPTION or impl.TITLE));
    end;

    _add('scroll_container_end[]');

    return table.concat(formspec, '');
end;


--- Shows the subtitle menu to the specified player.
-- @param player [ObjectRef|string] The player to show the formspec to.
function subtitles.show_menu(player)
    local formspec = subtitles.get_menu_formspec(player);
    if type(player) ~= 'string' then
        player = player:get_player_name();
    end;
    minetest.show_formspec(player, 'subtitles:menu', formspec);
end;


--- Handles receiving fields from the menu formspec.
-- @param player   [ObjectRef|string] The player the fields are being received from.
-- @param fields   [table]            A table of fields.
-- @param embedded [boolean]          If this is true, the menu will not be re-shown.
function subtitles.menu_receive_fields(player, fields, embedded)
    local function _reshow()
        if not embedded then
            subtitles.show_menu(player);
        end;
    end;

    local agent = subtitles.get_agent(player);

    if fields.toggle then
        agent:toggle_enabled();
        _reshow();
        return;
    end;

    if fields.footsteps then
        agent:toggle_footsteps_enabled();
        _reshow();
        return;
    end;

    for _, impl in ipairs(subtitles.SubtitleDisplay.implementations) do
        if fields['display_' .. impl.NAME] then
            agent:set_display_name(impl.NAME);
            _reshow();
            return;
        end;
    end;
end;


if not subtitles.settings.inventory_integration then
    return;
end;


-- Unified Inventory integration:
if minetest.get_modpath('unified_inventory') then
    unified_inventory.register_button('subtitles',
    {
        type      = 'image';
        image     = 'subtitles_icon.png';
        tooltip   = S'Subtitles';
        hide_lite = false;
        action    = subtitles.show_menu;
    });
end;


-- i3 integration:
if minetest.get_modpath('i3') then
    i3.new_tab('subtitles',
    {
        description = S'Subtitles';
        image       = 'subtitles_icon_flat_hd.png';
        formspec =
        function(player, data, fs)
            fs(subtitles.get_menu_formspec(player, true, {width = 10.25, height = 12}));
        end;
        fields =
        function(player, data, fields)
            subtitles.menu_receive_fields(player, fields, true);
            i3.set_fs(player);
        end;
    });
end;


-- SFInv integration via SFInv Buttons:
if minetest.get_modpath('sfinv_buttons') then
    sfinv_buttons.register_button('subtitles',
    {
        image   = 'subtitles_icon.png';
        tooltip = S'Configure your subtitle preferences';
        title   = S'Subtitles';
        action  = subtitles.show_menu;
    });
end;


-- Repixture integration:
if minetest.get_modpath('rp_formspec') then
    rp_formspec.register_page('subtitles:subtitles', 'label[1,1;?]');
    rp_formspec.register_invpage('subtitles:subtitles', {});
    rp_formspec.register_invtab('subtitles:subtitles', {icon = 'subtitles_rp_icon.png', tooltip = S'Subtitles'});

    local old_set_current_invpage = rp_formspec.set_current_invpage;
    function rp_formspec.set_current_invpage(player, page)
        if page == 'subtitles:subtitles' then
            subtitles.show_menu(player);
        else
            old_set_current_invpage(player, page);
        end;
    end;
end;
